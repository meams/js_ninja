const flatten = (arr) => {
    let fl = []
    arr.map(el => {
        if(el instanceof Array) {
            fl.push(...flatten(el))
        } else {
            fl.push(el)
        }
    })
    return fl
}

console.log(flatten([1, 2, {}, [3, [4], 5], [6, "seven"]]))