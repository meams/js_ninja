const calc = (firstNumber, secondNumber, operation, result) => {
    firstNumber = parseInt(firstNumber)
    secondNumber = parseInt(secondNumber)
    result = parseInt(result)

    switch (operation) {
        case '+':
            return firstNumber + secondNumber === result
        case '-':
            return firstNumber - secondNumber === result
        case '*':
            return firstNumber * secondNumber === result
        case '/':
            return firstNumber / secondNumber === result
        case '%':
            return firstNumber % secondNumber === result
        case '**':
            return firstNumber ** secondNumber === result
        default: return false
    }
}

console.log(calc(1, 2, '**', 1))