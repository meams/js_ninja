const groupWeeks = arr => {
    let resArr = []
    arr.reduce((acc, curVal) => {
        if (new Date(curVal.date).getDay() === 1) {
            acc = {}
            acc.weekStart = curVal.date
            acc.count = curVal.count
            resArr.push(acc)
        } else {
            acc.count = acc.count + curVal.count
        }
        return acc
    }, {})
    return resArr
}

console.log(groupWeeks([
    { date: '2019-04-08', count: 10 },
    { date: '2019-04-10', count: 23 },
    { date: '2019-04-22', count: 5 },
]))